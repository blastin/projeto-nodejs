// CONFIGURACAO INICIAL
const express = require("express")
const redis = require('redis')
const app = express()

// CONFIGURAÇÕES DE RENDERIZAÇÃO
app.set('view engine', 'ejs');
app.set('views', 'server/public')
app.use(express.static(__dirname + '/../public'))

const client = redis.createClient({
    host: process.env.redis_server,
    port: process.env.redis_port
})
client.set('visitas', 0)

// INICIAR SISTEMA
app.get('/', function (req, res) {
    client.get('visitas', (err, visitasRedis) => {
        var visitas = parseInt(visitasRedis)
        res.render('index', { version: process.env.npm_package_version, visitas: visitas })
        client.set('visitas', visitas + 1)
    })
})

// INICIAR SERVIDOR ESCUTANDO A PORTA 8080
app.listen(8080, function () {
    console.log("SERVIDOR ONLINE NA PORTA 8080")
})

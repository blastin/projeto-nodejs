# Projeto Node.JS com Kubernetes, utilizando plataforma de nuvem azure

![fluxo](https://gitlab.com/blastin/projeto-nodejs/uploads/1f723799827ad47af08e3366740f929a/Feature-Fluxo.png?raw=true)


## Primeiro Ato

    -   Criar grupo de recurso
    ```
        az group create --name nodeAKSGroup --location eastus --enable-addons http_application_routing
    ```

## Segundo Ato
    -   Criar um cluster para o projeto

    ```
        az aks create --resource-group nodeAKSGroup --name nodeCluster --node-count 1 --enable-addons monitoring --generate-ssh-keys
    ```

    -   Capturar DNS Zone
    ```
        az aks show --resource-group nodeAKSGroup --name nodeCluster --query addonProfiles.httpApplicationRouting.config.HTTPApplicationRoutingZoneName -o table
    ```

    -   Criar variavéis globais com os seguintes valores:

        -   resource-group
        -   cluster-name
        -   dns-zone

## Terceiro Ato
    -   Obter Credênciais de Azure Cloud

    ```
        az aks get-credentials --resource-group nodeAKSGroup --name nodeCluster
    ```

## Quarto Ato
    -   Criar uma restrição de acesso para login

    ```
        az ad sp create-for-rbac --name https://gitlab.com/blastin/projeto-cliente-nodejs
    ```
    
    - Criar variaveis globais com os seguintes valores respondidos:

        -   password
        -   tenant

## Quinto Ato
    -   Criar Pipeline para deploy da feature
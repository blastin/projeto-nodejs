
#!/bin/sh
#
# This file is part of projeto-cliente-nodej-js.
#
# projeto-cliente-nodej-js is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# projeto-cliente-nodej-js is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with projeto-cliente-nodej-js.  If not, see <https://www.gnu.org/licenses/>6.
#
# @Autor: Jefferson Lisboa <lisboa.jeff@gmail.com>
#
# RUN: ingress.sh *.yml nameproject name-ref-commit
#
#
#VARIAVEIS
FILE="$1"
NAME_APP="$2"
URL_APP="$3"
#
#
# COMANDOS SED
#
#
#SUBSTITUIR __NAME_APP__
sed -i "s|__NAME_APP__|$NAME_APP|g" "$FILE"
#
#SUBSTITUIR __URL_APP__
sed -i "s|__URL_APP__|$URL_APP|g" "$FILE"
#
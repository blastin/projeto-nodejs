
#!/bin/sh
#
# This file is part of projeto-node-js.
#
# projeto-node-js is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# projeto-node-js is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with projeto-node-js.  If not, see <https://www.gnu.org/licenses/>6.
#
# @Autor: Jefferson Lisboa <lisboa.jeff@gmail.com>
#
# RUN: deployment.sh *.yml name-project registry-image-dockername-ref-commit revision
#
#
#VARIAVEIS
FILE="$1"
NAME_APP="$2"
CI_REGISTRY_IMAGE="$3"
REVISION="$4"
#
#
# COMANDOS SED
#
#
#SUBSTITUIR __NAME_APP__
sed -i "s|__NAME_APP__|$NAME_APP|g" "$FILE"
#
#SUBSTITUIR __CI_REGISTRY_IMAGE__
sed -i "s|__CI_REGISTRY_IMAGE__|$CI_REGISTRY_IMAGE|g" "$FILE"
#
#SUBSTITUIR __REVISION__
sed -i "s|__REVISION__|$REVISION|g" "$FILE"
#